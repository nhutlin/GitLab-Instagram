const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const app = express();
const port = 3000;

// Bộ nhớ tạm thời để lưu trữ tài khoản người dùng (thay vì cơ sở dữ liệu thực)
const users = [];

app.use(bodyParser.json());

// Đăng ký tài khoản mới
app.post('/register', (req, res) => {
  const { username, password } = req.body;
  users.push({ username, password });
  res.send('Đăng ký thành công');
});

// Đăng nhập
app.post('/login', (req, res) => {
  const { username, password } = req.body;
  const user = users.find(u => u.username === username && u.password === password);
  if (user) {
    res.send('Đăng nhập thành công');
  } else {
    res.status(401).send('Tên đăng nhập hoặc mật khẩu không đúng');
  }
});

// Route dễ bị XSS
app.get('/greet', (req, res) => {
  const name = req.query.name;
  res.send(`<h1>Chào, ${name}!</h1>`);
});

app.listen(port, () => {
  console.log(`Ứng dụng đang chạy tại http://localhost:${port}`);
});
